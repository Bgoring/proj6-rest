# Laptop Service

from json import encoder
from flask import Flask, redirect, url_for, request, render_template
import flask
from flask.helpers import flash
from flask_restful import Resource, Api
from pymongo import MongoClient
import json
import pandas



client = MongoClient("db", 27017)
db = client.tododb

# Instantiate the app
app = Flask(__name__)
api = Api(app)

def new():
    _items = db.tododb.find()
    items = [item for item in _items]
    return items

class Brevet(Resource):
    def get(self):
         x = new()
         retj = []
         for i in range(len(x)):
            x[i].pop("_id")
            retj.append(x[i])

         return json.loads(json.dumps(retj))

    
class displayopen(Resource):
    def get(self):
         x = new()
         lenAll = len(x)
         k = request.args.getlist("top")
         if k != []:
            kval = int(k[0])
         else:
             kval = -1

         if kval > lenAll:
             return "The K val is greater than the number of database entries"
         if len(k) != 0:
            lenAll = int(k[0])
         retj = []
         for i in range(lenAll):
            x[i].pop("_id")
            x[i]["brevets"][0]["controls"][0].pop("close")
            retj.append(x[i])
         return json.loads(json.dumps(retj))

class displayclose(Resource):
    def get(self):
         x = new()
         lenAll = len(x)
         k = request.args.getlist("top")
         if k != []:
            kval = int(k[0])
         else:
             kval = -1

         if kval > lenAll:
             return "The K val is greater than the number of database entries"
         if len(k) != 0:
            lenAll = int(k[0])
         retj = []
         for i in range(lenAll):
            x[i].pop("_id")
            x[i]["brevets"][0]["controls"][0].pop("open")
            retj.append(x[i])
         return json.loads(json.dumps(retj))


class allCsv(Resource):

    def get(self):
         x = new()
         retj = []
         for i in range(len(x)):
            x[i].pop("_id")
            retj.append(x[i]) 
         df = pandas.DataFrame.from_dict(retj)
         return df.to_csv(index=False)

class openCsv(Resource):

    def get(self):
         x = new()
         lenAll = len(x)
         k = request.args.getlist("top")
         if k != []:
            kval = int(k[0])
         else:
             kval = -1

         if kval > lenAll:
             return "The K val is greater than the number of database entries"
         if len(k) != 0:
            lenAll = int(k[0])
         retj = []
         for i in range(lenAll):
            x[i].pop("_id")
            x[i]["brevets"][0]["controls"][0].pop("close")
            retj.append(x[i]) 
         df = pandas.DataFrame.from_dict(retj)
         return df.to_csv(index=False)


class closeCsv(Resource):

    def get(self):
         x = new()
         lenAll = len(x)
         k = request.args.getlist("top")
         if k != []:
            kval = int(k[0])
         else:
             kval = -1

         if kval > lenAll:
             return "The K val is greater than the number of database entries"
         if len(k) != 0:
            lenAll = kval
         retj = []
         for i in range(lenAll):
            x[i].pop("_id")
            x[i]["brevets"][0]["controls"][0].pop("open")
            retj.append(x[i]) 
         df = pandas.DataFrame.from_dict(retj)
         return df.to_csv(index=False)




# Create routes
# Another way, without decorators
api.add_resource(Brevet, '/', '/listAll', '/listAll/json')
api.add_resource(displayopen, '/listOpenOnly', '/listOpenOnly/json/')
api.add_resource(displayclose, '/listCloseOnly','/listCloseOnly/json/' )
api.add_resource(allCsv, '/listAll/csv')
api.add_resource(openCsv, '/listOpenOnly/csv/')
api.add_resource(closeCsv, '/listCloseOnly/csv/')



#api.add_resource(displayLaptop, "/listAll")

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
