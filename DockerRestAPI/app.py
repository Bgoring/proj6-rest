from flask import Flask, redirect, url_for, request, render_template
import flask
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging


###
# Globals
###


client = MongoClient("db", 27017)
db = client.tododb
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    #added the distance variable to the url
    distance = request.args.get("distance", type=int)
    date = request.args.get("bdate")
    time = request.args.get("btime")
    year = int(date[:4])
    day = int(date[8:10])
    month = int(date[5:7])
    hour = int(time[:2])
    minute = int(time[3:5])

    dt = arrow.get(year, month, day, hour, minute).isoformat()
    app.logger.debug("km={}".format(km))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    open_time = acp_times.open_time(km, distance, dt)
    close_time = acp_times.close_time(km, distance, dt)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#############

@app.route("/noinput")
def noinput():
    return render_template("noinput.html")

@app.route("/empty")
def empty():
    return render_template("empty.html")


@app.route('/new')
def new():
    if db.tododb.count() == 0:
        return redirect(url_for("empty"), 302)

    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)


@app.route('/store', methods=['POST', "GET"])
def store():
    disList = request.args.getlist("distance")
    miList = request.args.getlist("miles")
    kmList = request.args.getlist("km")
    openList = request.args.getlist("open")
    closeList = request.args.getlist("close")
    time = request.args.get("begin_time")
    stdate = request.args.get("begin_date")
    year = str(stdate[:4])
    location = request.args.getlist("location")

    if miList[0] == '':
        return redirect(url_for("noinput"), 302)

    for i in range(len(miList)):
        if miList[i] == '':
            break
        else:
            item_doc = {
                "brevets":[
                {
                    'distance': int(disList[0]),
                    "begin_date": arrow.get(stdate,"YYYY-MM-DD" ).format("YYYY-MM-DD"),
                    "begin_time": time,
                    "controls":[
                        {
                'km': float(kmList[i]),
                'mi': float(miList[i]),
                "location": str(location[i]),
                'open': arrow.get(year +openList[i], 'YYYYddd MM/D H:m').format("YYYY-MM-DTH:mm"),
                'close': arrow.get(year +closeList[i], 'YYYYddd MM/D H:m').format("YYYY-MM-DTH:mm"), 
                }
                    ]
                }
                ]}

            db.tododb.insert_one(item_doc)
        
    return redirect(url_for('index'))

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)