
Author: Blayne Goring
Contact: bgoring@uoregon.edu


Specifications for project6
The purpose of this project was to recreate the functionality of the ACP brevet control times calculator.
This calcultor does not consider the French variations in close time.
The implemented algorithm is based on the following chart->


Control location in km ---- Min speed in km ---- Max speed in km 0 - 200 15 34

201 - 400 15 32
401 - 600 15 30
601 - 1000 11.428 28
1001 - 1300 13.333 26

The algorithm uses the specified closed times for 200, 300, 400, 600, and 100km. Close of XX = HH:MM
Close of 200 = 13:30 Close of 300 = 20:00 Close of 400 = 27:00 Close of 600 = 40:00 Close of 1000 = 75:00
The calculator does not consider final control distances that excessively pass the brevet length. If a control's location exceeds a brevets length, the default time for the specified brevet length is returned.


This program stores brevet inputs in the database following the click of the submit button. After the submit button is clicked the page refreshes to notify the user that the data was sent to the database as well as to allow them to add more contol times if desired. When the user clicks the display button the database displays all the stored brevet information. The database never drops a record only creates new records. If there are no inputs to the database when you attempt to display it an error page will be returned. If there are no inputs when you attempt to submit to the database the user will be redirected to am error page. 

This program adds the functionality of a restful api that can return the collections stored in the pymongo database in json or csv format.